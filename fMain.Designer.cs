﻿namespace Individual_Task_CS
{
    partial class fMain
    {
        /// <summary>
        /// Обязательная переменная конструктора.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Освободить все используемые ресурсы.
        /// </summary>
        /// <param name="disposing">истинно, если управляемый ресурс должен быть удален; иначе ложно.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Код, автоматически созданный конструктором форм Windows

        /// <summary>
        /// Требуемый метод для поддержки конструктора — не изменяйте 
        /// содержимое этого метода с помощью редактора кода.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
            this.cActivity = new System.Windows.Forms.ComboBox();
            this.cHeartRate = new System.Windows.Forms.ComboBox();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.cHours = new System.Windows.Forms.TextBox();
            this.cMins = new System.Windows.Forms.TextBox();
            this.cWeight = new System.Windows.Forms.TextBox();
            this.cCalc = new System.Windows.Forms.Button();
            this.BtnExit = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // cActivity
            // 
            this.cActivity.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cActivity.FormattingEnabled = true;
            this.cActivity.Items.AddRange(new object[] {
            "Aerobics",
            "Baseball",
            "Basketball",
            "Boxing",
            "Football",
            "Cycling",
            "Rowing",
            "Skiing",
            "Swimming",
            "Tennis",
            "Volleyball"});
            this.cActivity.Location = new System.Drawing.Point(201, 21);
            this.cActivity.Name = "cActivity";
            this.cActivity.Size = new System.Drawing.Size(121, 21);
            this.cActivity.TabIndex = 0;
            // 
            // cHeartRate
            // 
            this.cHeartRate.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.cHeartRate.FormattingEnabled = true;
            this.cHeartRate.Items.AddRange(new object[] {
            "50",
            "60",
            "70",
            "80",
            "90"});
            this.cHeartRate.Location = new System.Drawing.Point(200, 130);
            this.cHeartRate.Name = "cHeartRate";
            this.cHeartRate.Size = new System.Drawing.Size(121, 21);
            this.cHeartRate.TabIndex = 9;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Lucida Console", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label1.Location = new System.Drawing.Point(12, 23);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(79, 15);
            this.label1.TabIndex = 1;
            this.label1.Text = "Activity";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Lucida Console", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label2.Location = new System.Drawing.Point(12, 62);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(61, 15);
            this.label2.TabIndex = 2;
            this.label2.Text = "Weight";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Lucida Console", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label3.Location = new System.Drawing.Point(12, 95);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(115, 15);
            this.label3.TabIndex = 4;
            this.label3.Text = "Event Length";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Lucida Console", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.label4.Location = new System.Drawing.Point(12, 132);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 15);
            this.label4.TabIndex = 5;
            this.label4.Text = "Maximum Heart Rate";
            // 
            // cHours
            // 
            this.cHours.Location = new System.Drawing.Point(201, 93);
            this.cHours.Name = "cHours";
            this.cHours.Size = new System.Drawing.Size(57, 20);
            this.cHours.TabIndex = 6;
            this.cHours.Text = "HOURS";
            this.cHours.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cHours.Enter += new System.EventHandler(this.CHours_Enter);
            // 
            // cMins
            // 
            this.cMins.Location = new System.Drawing.Point(264, 93);
            this.cMins.Name = "cMins";
            this.cMins.Size = new System.Drawing.Size(57, 20);
            this.cMins.TabIndex = 8;
            this.cMins.Text = "MIN";
            this.cMins.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            this.cMins.Enter += new System.EventHandler(this.CMins_Enter);
            // 
            // cWeight
            // 
            this.cWeight.Location = new System.Drawing.Point(201, 60);
            this.cWeight.Name = "cWeight";
            this.cWeight.Size = new System.Drawing.Size(121, 20);
            this.cWeight.TabIndex = 3;
            // 
            // cCalc
            // 
            this.cCalc.Font = new System.Drawing.Font("Lucida Console", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.cCalc.Location = new System.Drawing.Point(15, 168);
            this.cCalc.Name = "cCalc";
            this.cCalc.Size = new System.Drawing.Size(307, 28);
            this.cCalc.TabIndex = 10;
            this.cCalc.Text = "Calculate";
            this.cCalc.UseVisualStyleBackColor = true;
            this.cCalc.Click += new System.EventHandler(this.CCalc_Click);
            // 
            // BtnExit
            // 
            this.BtnExit.Font = new System.Drawing.Font("Lucida Console", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.BtnExit.Location = new System.Drawing.Point(15, 204);
            this.BtnExit.Name = "BtnExit";
            this.BtnExit.Size = new System.Drawing.Size(307, 28);
            this.BtnExit.TabIndex = 11;
            this.BtnExit.Text = "Exit";
            this.BtnExit.UseVisualStyleBackColor = true;
            this.BtnExit.Click += new System.EventHandler(this.BtnExit_Click);
            // 
            // fMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(339, 241);
            this.Controls.Add(this.BtnExit);
            this.Controls.Add(this.cCalc);
            this.Controls.Add(this.cWeight);
            this.Controls.Add(this.cMins);
            this.Controls.Add(this.cHours);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cHeartRate);
            this.Controls.Add(this.cActivity);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "fMain";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Carbohydrate Calculator";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.ComboBox cActivity;
        private System.Windows.Forms.ComboBox cHeartRate;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.TextBox cHours;
        private System.Windows.Forms.TextBox cMins;
        private System.Windows.Forms.TextBox cWeight;
        private System.Windows.Forms.Button cCalc;
        private System.Windows.Forms.Button BtnExit;
    }
}

