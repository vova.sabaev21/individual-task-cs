﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.IO;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Individual_Task_CS
{
    public partial class fMain : Form
    {
        static double CalcCalorie(string activity, double weight, int hour, int min)
        {
            double vConstant = 0;

            // Determine activity - bodyweight calories/minute constant is expressed in kg
            switch (activity)
            {
                case "Aerobics":
                    vConstant = 0.102;
                    break;

                case "Baseball":
                    vConstant = 0.075;
                    break;

                case "Basketball":
                    vConstant = 0.139;
                    break;

                case "Boxing":
                    vConstant = 0.168;
                    break;

                case "Cycling":
                    vConstant = 0.122;
                    break;

                case "Football":
                    vConstant = 0.131;
                    break;

                case "Rowing":
                    vConstant = 0.121;
                    break;

                case "Skiing":
                    vConstant = 0.142;
                    break;

                case "Swimming":
                    vConstant = 0.137;
                    break;

                case "Tennis":
                    vConstant = 0.128;
                    break;

                case "Volleyball":
                    vConstant = 0.081;
                    break;
            };

            // Total Calories Formula
            double Calorie = weight * vConstant * (hour * 60) + min;

            return Calorie;
        }

        static double CalcReqCarbCalorie(int HartRate, double totalCalorie, double weight)
        {
            double cHeartRate = 0;

            // Determine Heart Rate Constant
            switch (HartRate)
            {
                case 50:
                    cHeartRate = 0.11;
                    break;
                case 60:
                    cHeartRate = 0.28;
                    break;
                case 70:
                    cHeartRate = 0.44;
                    break;
                case 80:
                    cHeartRate = 0.62;
                    break;
                case 90:
                    cHeartRate = 0.71;
                    break;
            };

            // Required Carbohydrates Calories Formula
            double ReqCarbCalorie = (cHeartRate * totalCalorie) - (13 * weight);

            return ReqCarbCalorie;
        }

        static double ReqCarbCalorieHour(double ReqCarbCalorie, int hour, int min)
        {
            // Calculate Required Calories of Carbohydrates per Hour
            double ReqCarbCalorieHour = ReqCarbCalorie / (hour + (min / 60));

            return ReqCarbCalorieHour;
        }

        static double ReqCarbHourGram(double ReqCarbCalorieHour)
        {
            // Calculate Required Grams of Carbohydrate Per Hour
            double ReqCarbHourGram = ReqCarbCalorieHour / 4;

            return ReqCarbHourGram;
        }
        public fMain()
        {
            InitializeComponent();
        }

        private void BtnExit_Click(object sender, EventArgs e)
        {
            Application.Exit();
        }

        private void CCalc_Click(object sender, EventArgs e)
        {
            string activity;
            double weight;
            int hours, minutes, HeartRate;
            // getting program location
            // the file will be created in the folder where the program is located
            string file_name = "CalorieCalculator.txt";
            string path = $@"{Application.StartupPath}\{file_name}";
            //MessageBox.Show(program_path);
            FileInfo fi1 = new FileInfo(path);

            if (!double.TryParse(cWeight.Text, out weight) || !int.TryParse(cHours.Text, out hours) || !int.TryParse(cHeartRate.Text, out HeartRate) ||
                !int.TryParse(cHours.Text, out hours) || !((cMins.Text == "MIN" || cMins.Text == "") || int.TryParse(cMins.Text, out minutes)) ||
                String.IsNullOrEmpty(cActivity.SelectedItem.ToString()) || String.IsNullOrEmpty(cHeartRate.SelectedItem.ToString()))
            {
                MessageBox.Show("You entered a wrong data", "Smth went wrong...", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                activity = cActivity.SelectedItem.ToString();
                weight = double.Parse(cWeight.Text);
                hours = int.Parse(cHours.Text);
                minutes = (cMins.Text == "MIN" || cMins.Text == "") ? 0 : int.Parse(cMins.Text);
                HeartRate = int.Parse(cHeartRate.SelectedItem.ToString());

                double sumCalorie = CalcCalorie(activity, weight, hours, minutes);
                double sumReqCarbCalorie = CalcReqCarbCalorie(HeartRate, sumCalorie, weight);
                double sumReqCarbCalorieHour = ReqCarbCalorieHour(sumReqCarbCalorie, hours, minutes);
                double sumReqCarbHourGram = ReqCarbHourGram(sumReqCarbCalorieHour);

                MessageBox.Show($"You will need {sumCalorie} total calories.\n" +
                    $"You will need to ingest {sumReqCarbCalorie} total carbohydrate calories for your chosen % maximum heart rate.\n" +
                    $"You will need to ingest {sumReqCarbCalorieHour} carbohydrate calories/hour.\n" +
                    $"You will need to ingest {sumReqCarbHourGram} carbohydrate grams/hour." +
                    $"\n\n(1 carbohydrate gram = 4 Calories)", "Summary Information");

                if (!fi1.Exists)
                {
                    //Create a file to write to.
                    using (StreamWriter sw = new StreamWriter(path, false, System.Text.Encoding.Default))
                    {
                        sw.WriteLine($"You will need {sumCalorie} total calories.\n" +
                    $"You will need to ingest {sumReqCarbCalorie} total carbohydrate calories for your chosen % maximum heart rate.\n" +
                    $"You will need to ingest {sumReqCarbCalorieHour} carbohydrate calories/hour.\n" +
                    $"You will need to ingest {sumReqCarbHourGram} carbohydrate grams/hour." +
                    $"\n\n(1 carbohydrate gram = 4 Calories)\n");
                    }
                }

                MessageBox.Show($"Created file path {path}");
            }
        }

        private void CHours_Enter(object sender, EventArgs e)
        {
            if (cHours.Text == "HOURS") cHours.Text = "";
        }

        private void CMins_Enter(object sender, EventArgs e)
        {
            if (cMins.Text == "MIN") cMins.Text = "";
        }
    }
}
